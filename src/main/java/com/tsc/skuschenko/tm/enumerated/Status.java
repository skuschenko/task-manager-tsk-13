package com.tsc.skuschenko.tm.enumerated;

public enum Status {

    NOT_STARTED("not started"),
    IN_PROGRESS("in progress"),
    COMPLETE("complete");

    private String displayName;

    Status(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
