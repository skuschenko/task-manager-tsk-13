package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IProjectRepository;
import com.tsc.skuschenko.tm.api.repository.ITaskRepository;
import com.tsc.skuschenko.tm.api.service.IProjectTaskService;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService() {
    }

    public ProjectTaskService(
            final ITaskRepository taskService,
            final IProjectRepository projectRepository
    ) {
        this.taskRepository = taskService;
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final List<Task> allTasks = taskRepository.findAll();
        final List<Task> projectTasks = allTasks.stream()
                .filter(item -> projectId.equals(item.getProjectId()))
                .collect(Collectors.toList());
        return projectTasks;
    }

    @Override
    public Task bindTaskByProject(
            final String projectId, final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskFromProject(
            final String projectId, final String taskId
    ) {
        if (projectId == null || projectId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        final Project project = projectRepository.findOneById(projectId);
        if (project == null) return null;
        final Task task = taskRepository.findOneById(taskId);
        if (task == null || !task.getProjectId().equals(projectId)) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public Project deleteProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final List<Task> projectTasks = findAllTaskByProjectId(projectId);
        if (projectTasks != null && projectTasks.size() != 0)
            projectTasks.forEach(item -> taskRepository.remove(item));
        return projectRepository.removeOneById(projectId);
    }

    @Override
    public void clearProjects() {
        final List<Project> projects= projectRepository.findAll();
        if (projects != null && projects.size() != 0){
            projects.forEach(item ->  deleteProjectById(item.getId()));
        }
    }

}
